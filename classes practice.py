class Dog:
    #A simple class
    #attribute
    attr1 = "mamal"
    attr2 = "dog"

    def fun(self):
        print("I'm a", self.attr1)
        print("i'm a", self.attr2)

Rodger = Dog()
print(Rodger.attr1)
Rodger.fun()

# a sample class with init method
class Person:

    #init method or constructor
    def __init__(self, name):
        self.name = name
    
    #sample method
    def say_hi(self):
        print("Hello, my name is", self.name)

p = Person('Nikhil')
p.say_hi()


